package group.problem43;

/**
 * 43. Multiply Strings
 * 
 * Given two numbers represented as strings, return multiplication of the numbers as a string.
 * Note: The numbers can be arbitrarily large and are non-negative.
 * 
 * https://leetcode.com/problems/multiply-strings/
 * similar to problem2
 * 
 * */


public interface Solution {
    public String multiply(String num1, String num2);
}