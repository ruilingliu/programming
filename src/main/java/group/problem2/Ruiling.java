package group.problem2;

// Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)

public class Ruiling implements Solution {

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode result = l1;
		int increment = 0;
		
		while(l1!=null){
				int sum = l1.val+increment + (l2==null?0:l2.val);
				increment=0;
				int value = sum%10;
				increment = sum/10;
				l1.val= value;
				
				if(l1.next==null){
					if(l2!=null && l2.next!=null){
						l1.next=l2.next;
						l2.next=null;
					}else if(increment!=0){
							l1.next=new ListNode(increment);
							break;
					}
				}
				
				l1 = l1.next;
				l2=l2==null?null:l2.next;
				
				if(l2==null && increment==0){
					break;
				}
		}
		
		
		return result;
	}
	
	public void add(Integer data, ListNode ll){
		ListNode temp = new ListNode(data);
		ListNode current = ll;
		while(current.next!=null){
			current=current.next;
		}
		current.next=temp;
	}

	/**
	 * [1], [9,9]
	 * [5],[5]
	 * [1,8],[0]
	 * [9,8],[1]
	 * */
	public static void main(String[] args){
		Ruiling sol=new Ruiling();
		ListNode l1=new ListNode(9);
		sol.add(8, l1);
		
		ListNode l2=new ListNode(1);
		
		
		Zacharyhit sol2=new Zacharyhit();
		
		ListNode result = sol2.addTwoNumbers(l1, l2);
		
		ListNode current=result;
		while(current!=null){
			System.out.println(current.val);
			current = current.next;
		}
	}

}
