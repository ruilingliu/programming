package group.problem2;

public class Zacharyhit implements Solution {

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode ret = l1;
		int ct = 0;
		while( l1 != null ) {
			int sum = l1.val + ct + ( l2 == null ? 0 : l2.val );
			l1.val = sum % 10;
			ct = sum / 10;
			if( l1.next == null ) {
				if( l2 != null && l2.next != null ) {
					l1.next = l2.next;
					l2.next = null;
				} else if( ct != 0 ) {
					l1.next = new ListNode( ct );
					break;	
				}
			}
			l1 = l1.next;
			l2 = l2 == null ? null : l2.next;
			if( l2 == null && ct == 0 ) {
				break;
			}
		}
		return ret;		
	}

}
