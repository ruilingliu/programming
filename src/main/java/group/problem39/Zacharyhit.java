package group.problem39;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.rowset.CachedRowSet;

public class Zacharyhit implements Solution {
	List<Set<Integer>> cachedList = null;	
	public List<List<Integer>> combinationSum(int[] candidates, int target) {
		cachedList = new ArrayList<Set<Integer>>();
		for( int i = 0; i < candidates.length; i++ ) {
			cachedList.add( new HashSet<Integer>() );
		}

		Arrays.sort( candidates );		
		return combinationSum( candidates, 0, candidates.length, target );
	}
	
	public List<List<Integer>> combinationSum( int[] candidates, int start, int end, int target ) {
		List<List<Integer>> ret = new ArrayList<List<Integer>>();
		if( target == 0 ) {
			ret.add( new ArrayList<Integer>() );
			return ret;
		} else if( start + 1 == end ) {
			// leaf nodes;
			if( target % candidates[start] == 0 ) {
				List<Integer> oneRet = new ArrayList<Integer>();
				for( int i = 0; i < target / candidates[start]; i++ ) {
					oneRet.add( candidates[start] );
				}
				ret.add( oneRet );
			}
			return ret;
		}
		if( cachedList.get(end-1).contains( target ) ) {
			return ret;
		}
		int loop = target / candidates[ end - 1 ];
		for( int i = 0; i <= loop; i++ ) {
			int subTarget = target - i * candidates[ end - 1 ];
			List<List<Integer>> subRet = combinationSum( candidates, start, end - 1, subTarget );
			for( List<Integer> item : subRet ) {
				for( int j = 0; j < i; j++ ) {
					item.add( candidates[ end - 1 ] );
				}
				ret.add( item );
			}
		}
		if( ret.isEmpty() ) {
			cachedList.get(end-1).add( target );
		}
		return ret;		
	}
	
	public static void main( String[] argv ) {
		int[] candidates = {1, 2, 3, 4, 5};
		int target = 10;
		Zacharyhit s = new Zacharyhit();
		List<List<Integer>> ret = s.combinationSum(candidates, target);
		for( List<Integer> item : ret ) {
			for( Integer item2 : item ) {
				System.out.print( item2 + " " );
			}
			System.out.println("\n");
		}
	}
	

}
