package group.problem1;

/*
 * Given an array of integers, find two numbers such that they add up to a specific target number.
 * The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2. Please note that your returned answers (both index1 and index2) are not zero-based.
 * You may assume that each input would have exactly one solution.
 * Input: numbers={2, 7, 11, 15}, target=9
 * Output: index1=1, index2=2
 * */
public class Ruiling implements Solution {
	public int[] twoSum(int[] nums, int target) {
		int[] index = new int[2];
		int size = nums.length;

		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				if (nums[i] + nums[j] == target) {
					index[0] = i + 1;
					index[1] = j + 1;
					break;
				}
			}
		}
		return index;
	}

	public static void main(String[] args) {
		int[] numbers = { 2, 7, 11, 15 };
		int target = 13;

		Ruiling sol = new Ruiling();
		int[] result = sol.twoSum(numbers, target);

		System.out.println(result[0]);
		System.out.println(result[1]);
	}
}