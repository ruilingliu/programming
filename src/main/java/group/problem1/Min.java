package group.problem1;

import java.util.HashMap;

public class Min implements Solution {

	public int[] twoSum(int[] nums, int target) {
		// TODO Auto-generated method stub
		int[] result = new int[2];
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i=0; i<nums.length; i++){
			if (map.containsKey(nums[i])){
				result[0] = map.get(nums[i]);
				result[1] = i+1;
				return result;
			}
			map.put(target - nums[i], i+1);
		}
		return result;
	}
	
	public static void main(String[] args){
    	int[] numbers={2,5,1,4,2};
    	Min so = new Min();
    	int result[] = so.twoSum(numbers, 7);

    	System.out.println(result[0]);
    	System.out.println(result[1]);
	}
	
}
