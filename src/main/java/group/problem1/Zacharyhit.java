package group.problem1;

import java.util.HashMap;
import java.util.Map;

public class Zacharyhit implements Solution {

	public int[] twoSum(int[] nums, int target) {
		Map<Integer, Integer> numMap = new HashMap<Integer, Integer>();
		for( int i = 0; i < nums.length; i++ ) {
			int vi = nums[i];
			int vj = target - vi;
			if( numMap.containsKey( vj ) ) {
				return new int[] { numMap.get( vj ) + 1, i + 1 };
			} 		
			else if( !numMap.containsKey( nums[i] ) ) {
				numMap.put( nums[i], i);
			}
		}
		// no results
		return null;
	}

	public static void main( String[] argv ) {
		
		return;
	}
}
