package group.problem15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import group.problem15.Min;

public class Min implements Solution {
	/*
	public List<List<Integer>> threeSum(int[] nums) {
		// TODO Auto-generated method stub
		List<List<Integer>>results = new ArrayList<List<Integer>>();
		Arrays.sort(nums);
		
		for (int i=0; i<nums.length-3; i++){
			int start = i+1; 
			int end = nums.length - 1;
			while(start <end){
				int twosum = nums[start] + nums[end];
				if( twosum + nums[i] == 0 ){
					ArrayList<Integer> list = new ArrayList<Integer>();
					list.add(nums[start]);
					list.add(nums[i]);
					list.add(nums[end]);
					results.add(list);
					start ++;
					end --;
				}
				else
				if (twosum + nums[i] >0){
					end --;
				}
				else{
					start ++;
				}
					
				
				
			}
		}
		return results;
	}
	*/
	public List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> result = new ArrayList();
	    if(nums == null || nums.length < 3) return result;
	    Arrays.sort(nums);

	    int len = nums.length;
	    for(int i = 0; i < len; i++) {
	        if(i > 0 && nums[i] == nums[i - 1]) continue;        // Skip same results
	        int target = 0 - nums[i];
	        int j = i + 1, k = len - 1;
	        while(j < k) {
	            if(nums[j] + nums[k] == target) {
	                result.add(Arrays.asList(nums[i], nums[j], nums[k]));
	                while(j < k && nums[j] == nums[j + 1]) j++;  // Skip same results
	                while(j < k && nums[k] == nums[k - 1]) k--;  // Skip same results
	                j++; k--;
	            } else if(nums[j] + nums[k] < target) {
	                j++;
	            } else {
	                k--;
	            }
	        }
	    }
	    return result;
	}
	
	public static void main(String[] args){
    	int[] numbers={2,5,-3, 0, -7, 1, 4, -2, 8};
    	Min so = new Min();
    	for(List<Integer> s : so.threeSum(numbers)){
    		s= (ArrayList<Integer>)s;
    		for (int i=0; i<s.size(); i++){
    			System.out.print( s.get(i) + " ");
    		}
    		System.out.println("");
    	}

    	
	}
}
