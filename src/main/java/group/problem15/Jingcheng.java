package group.problem15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


/*
 * Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
 * Note:
 * Elements in a triplet (a,b,c) must be in non-descending order. (ie, a ≤ b ≤ c)
 * The solution set must not contain duplicate triplets. 
 * For example, given array S = {-1 0 1 2 -1 -4},
 * A solution set is:
 *   (-1, 0, 1)
 *   (-1, -1, 2)
 * 
 * https://leetcode.com/problems/3sum/
 * */

public class Jingcheng implements Solution {

	public List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		Set<String> resultSet = new HashSet<String>();
		int numsLength = nums.length;
		List<Integer> numList = new LinkedList<Integer>();
		for (int i = 0; i < numsLength; i++) {
			numList.add(nums[i]);
		}

		for (int i = 0; i < numsLength - 1; i++) {
			for (int j = i + 1; j < numsLength; j++) {
				int t = 0 - numList.get(i) - numList.get(j);
				List<Integer> tempList = new LinkedList<Integer>();
				tempList.clear();
				tempList.addAll(numList);
				tempList.remove(i);
				tempList.remove(j - 1);
				if (tempList.contains(t)) {

					String key = sortThree(new int[] { t, numList.get(i), numList.get(j) });

					resultSet.add(key.trim());
				}
			}
		}
		for (String r : resultSet) {
			String[] strs = r.split(" ");
			List<Integer> resultUnit = new ArrayList<Integer>();
			resultUnit.clear();
			resultUnit.add(Integer.parseInt(strs[0]));
			resultUnit.add(Integer.parseInt(strs[1]));
			resultUnit.add(Integer.parseInt(strs[2]));
			result.add(resultUnit);
		}

		return result;

	}

	public static String sortThree(int[] three) {

		if (three[0] > three[1]) {
			int t = three[0];
			three[0] = three[1];
			three[1] = t;
		}

		if (three[0] > three[2]) {
			int t = three[0];
			three[0] = three[2];
			three[2] = t;
		}

		if (three[1] > three[2]) {
			int t = three[1];
			three[1] = three[2];
			three[2] = t;
		}
		return three[0] + " " + three[1] + " " + three[2];

	}

	public static void main(String[] args) {
		int[] nums = new int[] { -1, 0, 1, 2, -1, -4, -3, 0, 0, 1000,-1000 };
		Jingcheng JC = new Jingcheng();
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		result = JC.threeSum(nums);
		for (int i = 0; i < result.size(); i++) {
			System.out.println(result.get(i));
		}

	}

}
