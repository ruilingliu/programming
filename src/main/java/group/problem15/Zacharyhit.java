package group.problem15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Zacharyhit implements Solution {
	
	public List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> ret = new ArrayList<List<Integer>>();
        if( nums.length <= 2 ) {
            return ret;
        }
		Arrays.sort( nums );
		for( int i = 0; i < nums.length - 2; i = nextHead( nums, i ) ) {
			int n1 = nums[i];
			int head = i + 1;
			int tail = nums.length - 1;
			int target = - n1;
			while( head < tail ) {
				int n2 = nums[ head ];
				int n3 = nums[ tail ];
				int sum = n2 + n3;
				if( sum == target ) {
					ret.add( Arrays.asList( n1, n2, n3 ) );
					head = nextHead( nums, head );
					tail = nextTail( nums, tail );
				} else if( sum < target ) {
					head = nextHead( nums, head );
				} else {
					tail = nextTail( nums, tail );
				}				
			}
		}
		return ret;
	}
	
	public int nextHead( int[] nums, int head ) {
		int n = nums[head];
		for( int i = head + 1; i < nums.length; i++ ) {
			if( nums[i] != n ) {
				return i;
			}
		}
		return nums.length;
	}
	
	public int nextTail( int[] nums, int tail ) {
		int n = nums[tail];
		for( int i = tail - 1; i >= 0; i-- ) {
			if( nums[i] != n ) {
				return i;
			}
		}
		return - 1;
	}
	
	public static void main( String[] argv ) {
		Solution s = new Zacharyhit();
		int[] nums = {1,-1,-1,0};
		for( List<Integer> line : s.threeSum( nums ) ) {
			System.out.println( line.get(0) + "\t" + line.get(1) + "\t" + line.get(2) );
		}
	}

}
