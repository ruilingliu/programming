package group.problem15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
 * Note:
 * Elements in a triplet (a,b,c) must be in non-descending order. (ie, a ≤ b ≤ c)
 * The solution set must not contain duplicate triplets. 
 * For example, given array S = {-1 0 1 2 -1 -4},
 * A solution set is:
 *   (-1, 0, 1)
 *   (-1, -1, 2)
 *   
 *   input: [3,-9,3,9,-6,-1,-2,8,6,-7,-14,-15,-7,5,2,-7,-4,2,-12,-7,-1,-2,1,-15,-13,-4,0,-9,-11,7,4,7,13,14,-7,-8,-1,-2,7,-10,-2,1,-10,6,-9,-1,14,2,-13,9,10,-7,-8,-4,-14,-5,-1,1,1,4,-15,13,-12,13,12,-11,12,-12,2,-3,-7,-14,13,-9,7,-11,5,-1,-2,-1,-7,-7,0,-7,-4,1,3,3,9,11,14,10,1,-13,8,-9,13,-2,-6,1,10,-5,-6,0,1,8,4,13,14,9,-2,-15,-13]
 * 
 * https://leetcode.com/problems/3sum/
 * */
public class Ruiling implements Solution {

	public List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> result= new ArrayList<List<Integer>>();
		Map<Integer,List<List<Integer>>> map = new HashMap<Integer,List<List<Integer>>>();	
		int size = nums.length;
		
		for(int i=0;i<size;i++){
			for(int j=i+1;j<size;j++){
				List<Integer> temp=new ArrayList<Integer>();
				if(nums[i]>=nums[j]){
					temp.add(nums[j]);
					temp.add(nums[i]);
				}else{
					temp.add(nums[i]);
					temp.add(nums[j]);
				}
				
				int sum=nums[i]+nums[j];
				
				if(!map.containsKey(sum)){
					List<List<Integer>> llList=new ArrayList<List<Integer>>();
					llList.add(temp);
					map.put(sum,llList);
				}else{
					List<List<Integer>> llList=new ArrayList<List<Integer>>();
					llList=map.get(sum);
					if(!llList.contains(temp)){
						llList.add(temp);
					}
					map.put(sum, llList);
				}
			}
		}
		
		
		// Put two numbers array into a hashmap
		// check the hashmap
		Set set=map.entrySet();
		Iterator i =set.iterator();
		
		while(i.hasNext()){
			Map.Entry me = (Map.Entry)i.next();
				
			int third=0-(Integer)me.getKey();

			if(contain(nums, third)){
			
				for (List<java.lang.Integer> object : (List<List<java.lang.Integer>>) me.getValue()) {
					object.add(third);
					Collections.sort(object);
					if(!result.contains(object)){
						result.add(object);
					}
					
				}
			}		
		}
		return result;
	}
	
	// third value in the nums array and the index of the index != i and j
	public boolean contain(int[] nums, int n){
		for(int index=0;index<nums.length;index++) {
			if(nums[index]==n){
			return true;
			}
		}
		return false;
	}

	public static void main(String[] args){
		int[] nums= new int[]{-1,0,1,2,-1,-4};
		
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		Ruiling sol = new Ruiling();
		result = sol.threeSum(nums);
		
		System.out.println(Arrays.toString(result.toArray()));
	}
}
