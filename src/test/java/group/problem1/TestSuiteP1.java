package group.problem1;

import java.lang.reflect.Modifier;

import group.util.TestUtil;
import junit.framework.Test;
import junit.framework.TestSuite;

public class TestSuiteP1 {

	public static Test suite() {
		TestSuite suite = new TestSuite();
	    
		// 1. get all the solutions;
		for( Class c : TestUtil.getClasses( "group.problem1" ) ) {
			if( Solution.class.isAssignableFrom( c ) && !Modifier.isAbstract( c.getModifiers() ) ) {
				// Solution implemented and not abstract class
				try {
					String[] fullName = c.getName().split( "\\." );
					String name = fullName[ fullName.length - 1 ];
					
					// test case 1, basic;
					suite.addTest( new TestCaseP1( name + " basic",
							(Solution) c.newInstance(),		// one of the solution;
							new int[] { 2, 7, 11, 15 },		// input: nums;
							9,								// input: target;
							new int[] { 1, 2 }				// output: answer;
							) );
					// test case 2, v(i) equals v(j);
					suite.addTest( new TestCaseP1( name + " i_equals_j",
							(Solution) c.newInstance(),		// one of the solution;
							new int[] { 2, 7, 7, 15 },		// input: nums;
							14,								// input: target;
							new int[] { 2, 3 }				// output: answer;
							) );
					
					// test case 3, very long nums
					int len = 50000;
					int[] nums = new int[ len ];
					for( int i = 0; i < nums.length; i++ ) {
						nums[i] = i + 1;
					}
					int ret = nums[ len - 2 ] + nums[ len - 1 ];
					suite.addTest( new TestCaseP1( name + " long nums",
							(Solution) c.newInstance(),		// one of the solution;
							nums,							// input: nums;
							ret,							// input: target;
							new int[] { len - 1, len }		// output: answer;
						));
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
			
	    return suite;
	}
}
