package group.problem1;

import junit.framework.TestCase;

public class TestCaseP1 extends TestCase {	
	Solution solution;
	int[] nums;
	int target;	
	int[] ans;

	public TestCaseP1( String name, Solution solution, int[] nums, int target, int[] ans ) {
		super( name );
		assertNotNull( solution );
		assertNotNull( nums );
		assertTrue( target > 0 );
		assertNotNull( ans );
		assertEquals( ans.length, 2 );
		
		assertTrue( ans[0] < ans[1] );
		assertTrue( ans[0] <= nums.length );
		assertTrue( ans[1] <= nums.length );
				
		assertEquals( target, nums[ ans[0] - 1 ] + nums[ ans[1] - 1 ] );
		
		this.solution = solution;
		this.nums = nums;
		this.target = target;
		this.ans = ans;		
	}

	protected void runTest() {
		
		int[] ret = solution.twoSum(nums, target);
		
		assertNotNull( ret );
		assertEquals( ret.length, 2 );
		assertTrue( ret[0] < ret[1] );
		assertTrue( ret[0] <= nums.length );
		assertTrue( ret[1] <= nums.length );
		assertEquals( nums[ ret[0] - 1 ] + nums[ret[1] - 1 ], target );
		assertEquals( ret[0], ans[0] );
		assertEquals( ret[1], ans[1] );
	}
	

}
