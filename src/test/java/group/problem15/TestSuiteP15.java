package group.problem15;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import group.util.TestUtil;
import junit.framework.Test;
import junit.framework.TestSuite;

public class TestSuiteP15 {

	public static Test suite() {
		TestSuite suite = new TestSuite();
	    
		// 1. get all the solutions;
		for( Class c : TestUtil.getClasses( "group.problem15" ) ) {
			if( Solution.class.isAssignableFrom( c ) && !Modifier.isAbstract( c.getModifiers() ) ) {
				// Solution implemented and not abstract class
				try {
					String[] fullName = c.getName().split( "\\." );
					String name = fullName[ fullName.length - 1 ];
					
					// test case 1, basic;
					ArrayList<List<Integer>> targetArrayList = new ArrayList<List<Integer>>();
					List<Integer> result1=new ArrayList<Integer>();
					result1.add(-1);
					result1.add(0);
					result1.add(1);
					targetArrayList.add(result1);
					
					List<Integer> result2=new ArrayList<Integer>();
					result2.add(-1);
					result1.add(-1);
					result1.add(2);
					targetArrayList.add(result2);
			
					
					suite.addTest( new TestCaseP15( name + " basic",
							(Solution) c.newInstance(),		// one of the solution;
							new int[] { -1, 0, 1, 2, -1, -4 },		// input: nums;
							targetArrayList	// output: answer;
							) );
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
			
	    return suite;
	}
}
