package group.problem15;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

public class TestCaseP15 extends TestCase {	
	Solution solution;
	int[] nums;
	List<List<Integer>> ans;

	public TestCaseP15( String name, Solution solution, int[] nums, List<List<Integer>> ans ) {
		super( name );
		assertNotNull( solution );
		assertNotNull( nums );
		assertNotNull( ans );
		assertEquals( ans.size(), 2 );
		
		this.solution = solution;
		this.nums = nums;
		this.ans = ans;		
	}

	protected void runTest() {
		
		List<List<Integer>> ret = solution.threeSum(nums);
		
		
		for(List<Integer> item:ret){
			assertTrue(item.size()==3);
			
			//triplet (a,b,c) must be in non-descending order
			assertTrue(item.get(0)<=item.get(1));
			assertTrue(item.get(1)<=item.get(2));
		}
		
		
		//solution set must not contain duplicate triplets
		Set<List<Integer>> set = new HashSet<List<Integer>>(ret);
		assertTrue(set.size()==ret.size());
	}
	

}
